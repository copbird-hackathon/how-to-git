# How to: Git

### Installation
- Linux/macos: https://git-scm.com/downloads
- Windows: https://gitforwindows.org
> "Git for Windows focuses on offering a lightweight, native set of tools that bring the full feature set of the Git SCM to Windows while providing appropriate user interfaces for experienced Git users and novices alike."

### Befehle
Die folgenden Befehle sind eine Auswahl der wichtigsten und am häufigsten verwendeten Befehle, natürlich gibt es weitaus mehr, siehe Tutorials.

- `git clone https://gitlab.com/copbird-hackathon/how-to-git.git` - Initiales Herunterladen eines Repositorys
- `git pull` - Aktualisieren eines existierenden Repositorys
- `git checkout <branch name>` - Wechseln auf einen anderen lokalen, existierenden Branch
- `git checkout -b <branch name>` - Erstellen eines neuen lokalen Branches vom aktuellen Branch
- `git add <file>` - Hinzufügen einer Datei zur Versionierung (Hinzufügen aller Dateien mit `git add .`)
- `git commit` - Commit eines aktuellen Stands
- `git push` - Upload aller Commits

### Tutorials
- `git help` (in der Kommandozeile oder in Git-Bash ausführen)
- Cheatsheet: https://education.github.com/git-cheat-sheet-education.pdf
- https://rogerdudler.github.io/git-guide
- https://git-scm.com/docs/gittutorial
- https://nvie.com/posts/a-successful-git-branching-model

